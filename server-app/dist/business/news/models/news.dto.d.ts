declare class HiglightResult {
    value: string;
    mathLevel: string;
    matchedWords: string[];
    fullyHighlighted?: boolean;
}
export declare class HiglightResultDto {
    author: HiglightResult;
    comment_text: HiglightResult;
    story_title: HiglightResult;
    story_url: HiglightResult;
}
export declare class NewsDto {
    created_at: string;
    title: string;
    url: string;
    author: string;
    points: string;
    story_text: string;
    comment_text: string;
    story_id: number;
    story_title: string;
    story_url: string;
    parent_id: string;
    created_at_i: number;
    _tags: string[];
    objectID: string;
    _highlightResult: HiglightResultDto;
}
export {};
