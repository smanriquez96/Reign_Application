"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NewsLogic = void 0;
const news_model_1 = require("../../api/models/news.model");
const common_1 = require("@nestjs/common");
const news_service_1 = require("../../services/news/news.service");
let NewsLogic = class NewsLogic {
    constructor(newsService) {
        this.newsService = newsService;
    }
    async getNews() {
        const news = await this.newsService.getNews();
        const filteredNews = news.filter((n) => n.storyTitle !== null || n.title !== null);
        const sortedNews = this.sortNews(filteredNews);
        return sortedNews;
    }
    sortNews(news) {
        return news.sort((a, b) => {
            if (new Date(a.createdAt) < new Date(b.createdAt))
                return 1;
            if (new Date(a.createdAt) > new Date(b.createdAt))
                return -1;
            return 0;
        });
    }
};
NewsLogic = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [news_service_1.NewsService])
], NewsLogic);
exports.NewsLogic = NewsLogic;
//# sourceMappingURL=news.logic.js.map