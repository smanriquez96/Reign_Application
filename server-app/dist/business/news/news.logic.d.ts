import { News } from "@api/models/news.model";
import { NewsService } from '@services/news/news.service';
export declare class NewsLogic {
    private readonly newsService;
    constructor(newsService: NewsService);
    getNews(): Promise<News[]>;
    private sortNews;
}
