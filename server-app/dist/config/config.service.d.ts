export declare class ConfigService {
    private readonly envConfig;
    constructor(filePath: string);
    get env(): string;
    get port(): number;
}
