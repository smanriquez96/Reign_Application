"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigService = void 0;
const dotenv = require("dotenv");
const fs = require("fs");
class ConfigService {
    constructor(filePath) {
        this.envConfig = dotenv.parse(fs.readFileSync(filePath));
    }
    get env() {
        return this.envConfig.NODE_ENV;
    }
    get port() {
        return Number(this.envConfig.NODE_PORT);
    }
}
exports.ConfigService = ConfigService;
//# sourceMappingURL=config.service.js.map