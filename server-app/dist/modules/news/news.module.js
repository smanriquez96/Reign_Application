"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NewsModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const news_model_1 = require("../../api/models/news.model");
const news_controller_1 = require("../../api/news/news.controller");
const news_logic_1 = require("../../business/news/news.logic");
const news_service_1 = require("../../services/news/news.service");
const http_interceptor_1 = require("../../interceptors/http/http.interceptor");
let NewsModule = class NewsModule {
};
NewsModule = __decorate([
    common_1.Module({
        imports: [
            common_1.HttpModule,
            mongoose_1.MongooseModule.forFeature([{ name: 'News', schema: news_model_1.NewsSchema }])
        ],
        controllers: [news_controller_1.NewsController],
        providers: [
            news_logic_1.NewsLogic,
            news_service_1.NewsService,
            http_interceptor_1.HttpInterceptor
        ],
    })
], NewsModule);
exports.NewsModule = NewsModule;
//# sourceMappingURL=news.module.js.map