"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_service_1 = require("./config/config.service");
const core_1 = require("@nestjs/core");
const news_service_1 = require("./services/news/news.service");
const app_module_1 = require("./app/app.module");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    const configService = app.get(config_service_1.ConfigService);
    app.enableCors();
    await app.listen(configService.port);
    const newsService = app.get(news_service_1.NewsService);
    newsService.newsTask();
}
bootstrap();
//# sourceMappingURL=main.js.map