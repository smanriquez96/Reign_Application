import { AxiosRequestConfig } from 'axios';
import { HttpService } from '@nestjs/common';
export declare class HttpInterceptor {
    private readonly http;
    constructor(http: HttpService);
    get(url: string, data?: any, config?: AxiosRequestConfig): Promise<any>;
    post(url: string, data: any, config?: AxiosRequestConfig, stringifyData?: boolean): Promise<any>;
}
