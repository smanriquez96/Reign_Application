import { ConfigService } from 'src/config/config.service';
import { VersionResponse } from './models/version.response';
export declare class AppService {
    private readonly config;
    constructor(config: ConfigService);
    getVersion(): Promise<VersionResponse>;
}
