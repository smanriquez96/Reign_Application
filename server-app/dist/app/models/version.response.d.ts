export declare class VersionResponse {
    env: string;
    version: string;
}
