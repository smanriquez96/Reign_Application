import { AppService } from './app.service';
import { VersionResponse } from './models/version.response';
export declare class AppController {
    private readonly appService;
    constructor(appService: AppService);
    getVersion(): Promise<VersionResponse>;
}
