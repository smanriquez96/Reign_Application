"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NewsService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const schedule_1 = require("@nestjs/schedule");
const mongoose_2 = require("mongoose");
const http_interceptor_1 = require("../../interceptors/http/http.interceptor");
const news_model_1 = require("../../api/models/news.model");
const news_dto_1 = require("../../business/news/models/news.dto");
let NewsService = class NewsService {
    constructor(http, newsModel) {
        this.http = http;
        this.newsModel = newsModel;
    }
    async getNews() {
        return this.getAllNews();
    }
    async newsTask() {
        console.log(`RETRIVING NEWS | LAST CHECKED: ${this.lastChecked}`);
        const url = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';
        this.http.get(url)
            .then(async (response) => {
            let newsToCreate = response.hits;
            if (this.lastChecked) {
                newsToCreate = newsToCreate.filter((news) => new Date(news.created_at) > this.lastChecked);
            }
            else {
                await this.getAllNews().then((response) => response.map((news) => this.deleteNews(news.id)));
            }
            newsToCreate.map((news) => this.createNews(news));
            this.lastChecked = new Date();
        })
            .catch(() => this.handleNewsError());
    }
    async createNews(news) {
        const newNews = new this.newsModel({
            points: news.points,
            createdAt: news.created_at,
            title: news.title,
            url: news.url,
            author: news.author,
            storyText: news.story_text,
            commentText: news.comment_text,
            storId: news.story_id,
            storyTitle: news.story_title,
            storyUrl: news.story_url,
            parentId: news.parent_id,
            createdAtI: news.created_at_i,
            tags: news._tags,
            objectId: news.objectID,
            highlightResult: news._highlightResult
        });
        const result = await newNews.save();
        return result.id;
    }
    async getAllNews() {
        return await this.newsModel.find().exec();
    }
    async deleteNews(id) {
        const result = await this.newsModel.deleteOne({ _id: id }).exec();
        if (result.n === 0) {
            throw new common_1.NotFoundException('Could not find news.');
        }
    }
    isNews(news) {
        return news.comment_text === null && !news._tags.includes('comment');
    }
    handleNewsError() {
        return [];
    }
};
__decorate([
    schedule_1.Cron(schedule_1.CronExpression.EVERY_HOUR),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], NewsService.prototype, "newsTask", null);
NewsService = __decorate([
    common_1.Injectable(),
    __param(1, mongoose_1.InjectModel('News')),
    __metadata("design:paramtypes", [http_interceptor_1.HttpInterceptor,
        mongoose_2.Model])
], NewsService);
exports.NewsService = NewsService;
//# sourceMappingURL=news.service.js.map