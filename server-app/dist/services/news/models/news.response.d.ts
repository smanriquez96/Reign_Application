import { NewsDto } from "@business/news/models/news.dto";
export declare class NewsResponse {
    hits: NewsDto[];
    exhaustiveNbHits: boolean;
    hitsPerPage: number;
    nbHits: number;
    nbPages: number;
    page: number;
    params: string;
    processingTimeMS: number;
    query: string;
}
