import { Model } from 'mongoose';
import { HttpInterceptor } from 'src/interceptors/http/http.interceptor';
import { News } from '@api/models/news.model';
export declare class NewsService {
    private readonly http;
    private readonly newsModel;
    private lastChecked;
    constructor(http: HttpInterceptor, newsModel: Model<News>);
    getNews(): Promise<News[]>;
    newsTask(): Promise<void>;
    private createNews;
    private getAllNews;
    private deleteNews;
    private isNews;
    private handleNewsError;
}
