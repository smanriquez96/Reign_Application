import { NewsLogic } from '@business/news/news.logic';
import { News } from "@api/models/news.model";
export declare class NewsController {
    private readonly newsLogic;
    constructor(newsLogic: NewsLogic);
    getNews(): Promise<News[]>;
}
