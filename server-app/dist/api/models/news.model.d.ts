import * as mongoose from 'mongoose';
declare class HiglightResult {
    value: string;
    mathLevel: string;
    matchedWords: string[];
    fullyHighlighted?: boolean;
}
declare class HiglightResultDto {
    author: HiglightResult;
    commentText: HiglightResult;
    storyTitle: HiglightResult;
    storyUrl: HiglightResult;
}
export declare const NewsSchema: mongoose.Schema<mongoose.Document<any, any, any>, mongoose.Model<any, any, any>, undefined, any>;
export interface News extends mongoose.Document {
    id: string;
    points: number;
    createdAt: string;
    title: string;
    url: string;
    author: string;
    storyText: string;
    commentText: string;
    storId: number;
    storyTitle: string;
    storyUrl: string;
    parentId: string;
    createdAtI: number;
    tags: string[];
    objectId: string;
    highlightResult: HiglightResultDto;
}
export {};
