"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NewsSchema = void 0;
const mongoose = require("mongoose");
class HiglightResult {
}
class HiglightResultDto {
}
exports.NewsSchema = new mongoose.Schema({
    name: { type: String },
    points: { type: Number },
    createdAt: { type: String },
    title: { type: String },
    url: { type: String },
    author: { type: String },
    storyText: { type: String },
    commentText: { type: String },
    storId: { type: Number },
    storyTitle: { type: String },
    storyUrl: { type: String },
    parentId: { type: String },
    createdAtI: { type: Number },
    tags: { type: Array },
    objectID: { type: String },
    highlightResult: {
        author: {
            value: { type: String },
            mathLevel: { type: String },
            matchedWords: { type: Array },
            fullyHighlighted: { type: Boolean }
        },
        commentText: {
            value: { type: String },
            mathLevel: { type: String },
            matchedWords: { type: Array },
            fullyHighlighted: { type: Boolean }
        },
        storyTitle: {
            value: { type: String },
            mathLevel: { type: String },
            matchedWords: { type: Array },
            fullyHighlighted: { type: Boolean }
        },
        storyUrl: {
            value: { type: String },
            mathLevel: { type: String },
            matchedWords: { type: Array },
            fullyHighlighted: { type: Boolean }
        }
    }
});
//# sourceMappingURL=news.model.js.map