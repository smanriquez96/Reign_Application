import { ConfigService } from '@config/config.service';
import { NestFactory } from '@nestjs/core';
import { NewsService } from '@services/news/news.service';
import { AppModule } from './app/app.module';


async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  
  app.enableCors();
  await app.listen(configService.port);
  const newsService = app.get(NewsService);
  newsService.newsTask();
}
bootstrap();
