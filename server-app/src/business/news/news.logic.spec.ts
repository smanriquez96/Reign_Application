import { Test, TestingModule } from "@nestjs/testing";
import { NewsService } from "@services/news/news.service";
import { NEWS_MOCK } from "./models/news.mock";
import { NewsLogic } from "./news.logic";

describe('NewsLogic', () => {
    let module: TestingModule;
    let logic: NewsLogic;
  
    beforeEach(async () => {
      module = await Test.createTestingModule({
        providers: [
            NewsLogic,
          { provide: NewsService, useValue: { getNews: () => { } } },
        ],
      }).compile();
  
      logic = module.get<NewsLogic>(NewsLogic);
    });
  
    it('should be defined.', () => {
      expect(logic).toBeDefined();
    });

    describe('getNews', () => {
      it('should return ordered and filtered news', async () => {
        const expected = [
          {
            _id: '1236',
            points: 0,
            createdAt: '2021-09-15T10:49:08.000Z',
            title: 'news title',
            url: 'https://example-url.com',
            author: 'News author',
            storyText: null,
            commentText: null,
            storId: 3213213,
            storyTitle: 'node js news',
            storyUrl: 'https://example-url.com',
            parentId: 345678,
            createdAtI: 1626346148,
            tags: [ ' story' ],
            objectId: '32163',
            highlightResult: null,
          },
          {
            _id: '1235',
            points: 0,
            createdAt: '2021-07-15T10:49:08.000Z',
            title: 'news title',
            url: 'https://example-url.com',
            author: 'News author',
            storyText: null,
            commentText: null,
            storId: 3213213,
            storyTitle: 'node js news',
            storyUrl: 'https://example-url.com',
            parentId: 345678,
            createdAtI: 1626346148,
            tags: [ ' story' ],
            objectId: '32163',
            highlightResult: null,
          },
        ];
        spyOn(logic['newsService'], 'getNews').and.returnValue(Promise.resolve([ ...NEWS_MOCK ]));
        const news = await logic.getNews();
        expect(news).toEqual(expected);
      });
    });
});
