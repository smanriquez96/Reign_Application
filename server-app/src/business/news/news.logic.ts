import { News } from "@api/models/news.model";
import { Injectable } from "@nestjs/common";
import { NewsService } from '@services/news/news.service';

@Injectable()
export class NewsLogic {
    constructor(
        private readonly newsService: NewsService
    ) { }

    public async getNews(): Promise<News[]> {
        const news = await this.newsService.getNews();
        const filteredNews = news.filter((n) => n.storyTitle !== null || n.title !== null);
        const sortedNews = this.sortNews(filteredNews);
        return sortedNews;
    }

    private sortNews(news: News[]): News[] {
        return news.sort((a: News, b: News) => {
          if (new Date(a.createdAt) < new Date (b.createdAt)) return 1;
          if (new Date(a.createdAt) > new Date (b.createdAt)) return -1;
          return 0;
        });
    }
}