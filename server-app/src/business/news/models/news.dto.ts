class HiglightResult {
    public value: string;
    public mathLevel: string;
    public matchedWords: string[];
    public fullyHighlighted?: boolean;

}

export class HiglightResultDto {
    public author: HiglightResult;
    public comment_text: HiglightResult;
    public story_title: HiglightResult;
    public story_url: HiglightResult;
}


export class NewsDto {
    public created_at: string;
    public title: string;
    public url: string;
    public author: string;
    public points:string;
    public story_text: string;
    public comment_text: string;
    public story_id: number;
    public story_title: string;
    public story_url: string;
    public parent_id: string;
    public created_at_i: number;
    public _tags: string[];
    public objectID: string;
    public _highlightResult: HiglightResultDto;
}
