import { of } from 'rxjs';
import { HttpService } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { HttpInterceptor } from './httpInterceptor';


describe('HttpInterceptor', () => {
  let module: TestingModule;
  let interceptor: HttpInterceptor;

  beforeEach(async () => {
    module = await Test.createTestingModule({
      providers: [
        HttpInterceptor,
        { provide: HttpService, useValue: { get: {}, post: {} } },
      ],
    }).compile();

    interceptor = module.get<HttpInterceptor>(HttpInterceptor);
  });

  it('should be defined.', () => {
    expect(interceptor).toBeDefined();
  });

  describe('get', () => {
    it('should return the data.', async () => {
      const getSpy = spyOn(interceptor['http'], 'get').and.returnValue(of({ data: { test: 123 } }));
      expect(await interceptor.get('www.example.com')).toEqual({ test: 123 });
      expect(getSpy).toHaveBeenCalledWith('www.example.com', { params: {} });
    });

    it('should use query params.', async () => {
      const getSpy = spyOn(interceptor['http'], 'get').and.returnValue(of({ data: { test: 123 } }));
      expect(await interceptor.get('www.example.com', { test: '123' })).toEqual({ test: 123 });
      expect(getSpy).toHaveBeenCalledWith('www.example.com', { params: { test: '123' } });
    });
  });

  describe('post', () => {
    it('post should return the data.', async () => {
      const postSpy = spyOn(interceptor['http'], 'post').and.returnValue(of({ data: { test: 123 } }));
      expect(await interceptor.post('', {})).toEqual({ test: 123 });
      expect(postSpy).toHaveBeenCalledWith('', '', undefined);
    });

    it('post should return the data and not convert the body into a string.', async () => {
      const postSpy = spyOn(interceptor['http'], 'post').and.returnValue(of({ data: { test: 123 } }));
      expect(await interceptor.post('', {}, null, false)).toEqual({ test: 123 });
      expect(postSpy).toHaveBeenCalledWith('', {}, null);
    });
  });
});

