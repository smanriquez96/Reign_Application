import * as queryString from 'querystring';
import { AxiosRequestConfig } from 'axios';
import { map } from 'rxjs/operators';
import { Injectable, HttpService } from '@nestjs/common';

@Injectable()
export class HttpInterceptor {

  constructor(private readonly http: HttpService) { }

  public get(url: string, data?, config?: AxiosRequestConfig): Promise<any> {
    config = { ...config, params: { ...data } };
    return this.http.get<any>(url, config).pipe(map((response) => response.data)).toPromise();
  }

  public post(url: string, data, config?: AxiosRequestConfig, stringifyData = true): Promise<any> {
    if (stringifyData) data = queryString.stringify(data);
    return this.http.post<any>(url, data, config).pipe(map((response) => response.data)).toPromise();
  }
}
