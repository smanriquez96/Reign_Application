import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { NewsSchema } from '@api/models/news.model';
import { NewsController } from '@api/news/news.controller';
import { NewsLogic } from '@business/news/news.logic';
import { NewsService } from '@services/news/news.service';
import { HttpInterceptor } from '@interceptors/http/httpInterceptor';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([{ name: 'News', schema: NewsSchema }])
  ],
  controllers: [NewsController],
  providers: [
      NewsLogic,
      NewsService,
      HttpInterceptor
],
})
export class NewsModule { }
