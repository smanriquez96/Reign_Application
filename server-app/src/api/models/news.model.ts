import * as mongoose from 'mongoose';

class HiglightResult {
    value: string;
    mathLevel: string;
    matchedWords: string[];
    fullyHighlighted?: boolean;

}

class HiglightResultDto {
    author: HiglightResult;
    commentText: HiglightResult;
    storyTitle: HiglightResult;
    storyUrl: HiglightResult;
}

export const NewsSchema = new mongoose.Schema({
  name: { type: String },
  points: { type: Number },
  createdAt: { type: String },
  title: { type: String },
  url: { type: String },
  author: { type: String },
  storyText: { type: String },
  commentText: { type: String },
  storId: { type: Number },
  storyTitle: { type: String },
  storyUrl: { type: String },
  parentId: { type: String },
  createdAtI: { type: Number },
  tags: { type: Array },
  objectID: { type: String },
  highlightResult: {
    author: {
        value: { type: String },
        mathLevel: { type: String },
        matchedWords: { type: Array },
        fullyHighlighted: { type: Boolean }
    },
    commentText: {
        value: { type: String },
        mathLevel: { type: String },
        matchedWords: { type: Array },
        fullyHighlighted: { type: Boolean }
    },
    storyTitle: {
        value: { type: String },
        mathLevel: { type: String },
        matchedWords: { type: Array },
        fullyHighlighted: { type: Boolean }
    },
    storyUrl: {
        value: { type: String },
        mathLevel: { type: String },
        matchedWords: { type: Array },
        fullyHighlighted: { type: Boolean }
    }
  }
});

export interface News extends mongoose.Document {
    id: string;
    points: number;
    createdAt: string;
    title: string;
    url: string;
    author: string;
    storyText: string;
    commentText: string;
    storId: number;
    storyTitle: string;
    storyUrl: string;
    parentId: string;
    createdAtI: number;
    tags: string[];
    objectId: string;
    highlightResult: HiglightResultDto;
}
