import { Controller, Get, HttpCode } from "@nestjs/common";
import { NewsLogic } from '@business/news/news.logic';
import { News } from "@api/models/news.model";


@Controller('news')
export class NewsController {

  constructor(
    private readonly newsLogic: NewsLogic
  ) { }

  @Get()
  @HttpCode(200)
  public async getNews(): Promise<News[]>  {
    return await this.newsLogic.getNews();
  }
}
