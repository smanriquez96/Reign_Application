import { Test, TestingModule } from '@nestjs/testing';
import { NewsLogic } from '@business/news/news.logic';
import { NewsController } from './news.controller';

describe('TerminalPatientsController', () => {
  let controller: NewsController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [NewsController],
      providers: [
        {
          provide: NewsLogic, useValue: {
            getNews: () => { }
          }
        },
      ]
    }).compile();

    controller = app.get<NewsController>(NewsController);
  });

  it('should be defined.', () => {
    expect(controller).toBeDefined();
  });
});
