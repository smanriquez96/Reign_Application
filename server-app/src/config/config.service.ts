import * as dotenv from 'dotenv';
import * as fs from 'fs';

export class ConfigService {

  private readonly envConfig: { [key: string]: string };

  constructor(filePath: string) {
    this.envConfig = dotenv.parse(fs.readFileSync(filePath));
  }

  public get env(): string {
    return this.envConfig.NODE_ENV;
  }

  public get port(): number {
    return Number(this.envConfig.NODE_PORT);
  }
}
