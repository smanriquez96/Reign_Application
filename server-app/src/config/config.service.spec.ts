/* tslint:disable no-magic-numbers */
import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from './config.service';

describe('Config', () => {
  let provider: ConfigService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: ConfigService,
          useValue: new ConfigService('./overlays/test/.env'),
        },
      ],
    }).compile();
    provider = module.get(ConfigService);
  });

  it('should be defined.', () => {
    expect(provider).toBeDefined();
  });

  it('getport should return the port.', () => {
    expect(provider.port).toEqual(8080);
  });

  it('getenv should return the environment.', () => {
    expect(provider.env).toEqual('TEST');
  });
});
