import { Module } from '@nestjs/common';
import { ConfigService } from './config.service';

const env = process.env.NODE_ENV ? process.env.NODE_ENV.toLowerCase() : 'local';

@Module({
  providers: [
    {
      provide: ConfigService, useValue: new ConfigService(`./overlays/${env}/.env`)
    },
  ],
  exports: [ConfigService],
})
export class ConfigModule { }
