import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { ConfigModule } from 'src/config/config.module';
import { HttpInterceptor } from '@interceptors/http/httpInterceptor';
import { NewsModule } from 'src/modules/news/news.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';

const url = `mongodb://${process.env.MONGO_HOSTNAME}:${process.env.MONGO_PORT}/news`

@Module({
  imports: [
    HttpModule,
    ConfigModule,
    ScheduleModule.forRoot(),
    MongooseModule.forRoot(url, { auth: { user: process.env.MONGO_USER, password: process.env.MONGO_PASSWORD }, useNewUrlParser: true }),
    NewsModule
  ],
  controllers: [
    AppController,
  ],
  providers: [
    AppService,
    HttpInterceptor
  ],
})
export class AppModule {}
