import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { VersionResponse } from './models/version.response';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  public getVersion(): Promise<VersionResponse> {
    return this.appService.getVersion();
  }
}
