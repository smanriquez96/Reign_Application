import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';

describe('AppController', () => {
  let controller: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [{ provide: AppService, useValue: { getVersion: () => { } } }]
    }).compile();

    controller = app.get<AppController>(AppController);
  });

  describe('getVersion', () => {
    it('should return the version of the project.', () => {
      const expected = { env: 'test', version: '0.0.0'} ;
      const getVersionSpy = spyOn(controller['appService'], 'getVersion').and.returnValue(expected);
      expect(controller.getVersion()).toBe(expected);
      expect(getVersionSpy).toHaveBeenCalled();
    });
  });
});
