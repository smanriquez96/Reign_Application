import { Injectable } from '@nestjs/common';
import { ConfigService } from '@config/config.service';
import { VersionResponse } from './models/version.response';

@Injectable()
export class AppService {

  constructor(private readonly config: ConfigService) { }

  public async getVersion(): Promise<VersionResponse> {
    return {
      env: this.config.env,
      version: process.env.npm_package_version
    } as VersionResponse;
  }
}
