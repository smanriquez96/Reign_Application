import { HttpInterceptor } from "@interceptors/http/httpInterceptor";
import { Test, TestingModule } from "@nestjs/testing";
import { NewsService } from "./news.service";
import { ConfigService } from "@config/config.service";
import { getModelToken } from "@nestjs/mongoose";
import { NewsModelMock } from "./models/news.mocks";
import { NEWS_RESPONSE_MOCK } from "./models/news.response";

describe('NewsService', () => {
  let module: TestingModule;
  let service: NewsService;

  beforeEach(async () => {
    module = await Test.createTestingModule({
      providers: [
        NewsService,
        { provide: HttpInterceptor, useValue: { post: () => { }, get: () => { } } },
        { provide: ConfigService, useValue: {} },
        {
          provide: getModelToken('News'),
          useClass: NewsModelMock,
        },

      ],
    }).compile();

    service = module.get<NewsService>(NewsService);
  });

  it('should be defined.', () => {
    expect(service).toBeDefined();
  });

  describe('newsTask', () => {
    let httpGetSpy: jasmine.Spy;

    beforeEach(() => {
      httpGetSpy = spyOn(service['http'], 'get');
    });

    it('should return create news on the databse from the fetched ones', async () => {
      const createSpy = spyOn<any>(service, 'createNews');
      httpGetSpy.and.returnValue(Promise.resolve(NEWS_RESPONSE_MOCK));
      spyOn(service, 'getNews').and.returnValue(Promise.resolve([]));
      await service.newsTask();
      expect(createSpy).toHaveBeenCalled();
      expect(createSpy).toBeCalledWith(NEWS_RESPONSE_MOCK.hits[0])
    });

    it('should not create news', async () => {
      const createSpy = spyOn<any>(service, 'createNews');
      httpGetSpy.and.returnValue(Promise.reject());
      spyOn(service, 'getNews').and.returnValue(Promise.resolve([]));
      await service.newsTask();
      expect(createSpy).not.toHaveBeenCalled();
    });
  })
});
