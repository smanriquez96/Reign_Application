import { NewsDto } from "@business/news/models/news.dto";

export class NewsResponse {
    public hits: NewsDto[];
    public exhaustiveNbHits: boolean;    ​
    public hitsPerPage: number;
    public nbHits: number;
    public nbPages: number;
    public page: number;
    public params: string;
    public processingTimeMS: number;
    public query: string;
}

export const NEWS_RESPONSE_MOCK = {
    hits: [
        {
            created_at: '2021-07-15T10:49:08.000Z',
            title: 'news title',
            url: 'story_url',
            author: 'author',
            points: 0,
            story_text: null,
            comment_text: null,
            story_id: '21321321',
            story_title: 'node js news',
            story_url: 'example_url',
            parent_id: '213421421',
            created_at_i: 1626346148,
            _tags: [],
            objectID: '3213213',
            _highlightResult: null,
        }
    ]
};
