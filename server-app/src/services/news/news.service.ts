import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Model } from 'mongoose';
import { HttpInterceptor } from '@interceptors/http/httpInterceptor';
import { NewsResponse } from './models/news.response';
import { News } from '@api/models/news.model';
import { NewsDto } from '@business/news/models/news.dto';

@Injectable()
export class NewsService {
  private lastChecked: Date;

  constructor(
    private readonly http: HttpInterceptor,
    @InjectModel('News') private readonly newsModel: Model<News>
  ) { }

  public async getNews(): Promise<News[]> {
    return this.getAllNews();
  }

  @Cron(CronExpression.EVERY_HOUR)
  public async newsTask() {
    console.log(`RETRIEVING NEWS | LAST CHECKED: ${this.lastChecked}`);
    const url = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';
    const dbNews = await this.getNews();
    const dbNewsIds = dbNews.map((news) => news.objectId);

    this.http.get(url)
    .then(async (response: NewsResponse) => {
      let newsToCreate = response.hits;
      if (this.lastChecked) {
        newsToCreate = newsToCreate.filter((news) => new Date(news.created_at) > this.lastChecked);
      }
      newsToCreate = newsToCreate.filter(((news) => !dbNewsIds.includes(news.objectID)));
      newsToCreate.map((news) => this.createNews(news));
    })
    .catch(() => this.handleNewsError());
  }

  private async createNews(news: NewsDto) {
    const newNews = new this.newsModel({
      points: news.points,
      createdAt: news.created_at,
      title: news.title,
      url: news.url,
      author: news.author,
      storyText: news.story_text,
      commentText: news.comment_text,
      storId: news.story_id,
      storyTitle: news.story_title,
      storyUrl: news.story_url,
      parentId: news.parent_id,
      createdAtI: news.created_at_i,
      tags: news._tags,
      objectId: news.objectID,
      highlightResult: news._highlightResult
    });
    const result = await newNews.save();
    return result.id as string;
  }

  private async getAllNews(): Promise<News[]> {
    return await this.newsModel.find().exec();
  }

  private async deleteNews(id: string) {
    const result = await this.newsModel.deleteOne({ _id: id }).exec();
    if (result.n === 0) {
      throw new NotFoundException('Could not find news.');
    }
  }

  private isNews(news: NewsDto) {
    return news.comment_text === null && !news._tags.includes('comment');
  }

  private handleNewsError() {
    return [];
  }
}
