## Sample Application

This is a sample implementation of a News Feed.

This project is made of 3 separate Docker containers that holds:

- MongoDB database
- NestJS Backend
- Angular frontend

The entry point for a user is a website which is available under the address: **http://localhost:4200/**

---

### Prerequisites

In order to run this application you need to install two tools: **Docker** & **Docker Compose**.

Instructions how to install **Docker** on [Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/), [Windows](https://docs.docker.com/docker-for-windows/install/), [Mac](https://docs.docker.com/docker-for-mac/install/).

**Docker Compose** is already included in installation packs for *Windows* and *Mac*, so only Ubuntu users needs to follow [this instructions](https://docs.docker.com/compose/install/).


### How to run it?

Clone this repository:

```
$ git clone <repository_url>
```

Navigate to the project's root folder:

```
$ cd Reign_Application/
```

The entire application can be run with a single command on a terminal:

```
$ docker-compose up -d
```

If you want to stop it, use the following command:

```
$ docker-compose down
```

---

#### mongodb (Database)

MongoDB database contains a single schema News.
This database is containerized and the definition of its Docker container can be found in *docker-compose.yml* file.

```yml
mongodb:
    image: mongo
    container_name: mongodb
    volumes:
        - ./server-app/init-mongo.js:/docker-entrypoint-initdb.d/init-mongo.js:ro
        - mongo-data:/data/db
    restart: always
    ports:
        - 27017:27017
    environment:
        MONGO_INITDB_ROOT_USERNAME: admin
        MONGO_INITDB_ROOT_PASSWORD: example
        MONGO_INITDB_DATABASE: news
```

#### server-app (REST API)

NestJS based application that connects with a
database that and expose the REST endpoints that can be consumed by
frontend.  In this case there's only one endpoint /news thar
can be acceced by HTTP REST method GET and returns and orderd array of news.

The server-app is put in Docker container and its definition can be found
in a file *server-app/Dockerfile*. 
#### client-app (Frontend)

Angular single page application that can be accesed by the users where they can erase and click on news.
It consumes the REST API endpoint provided by *server-app*.

The client-app is put in Docker container and its definition can be found
in a file *client-app/Dockerfile*.

It can be entered using link: **http://localhost:4200/**
