# Client App

## Requirements

Before start, please make sure you have installed this programs.

* Node JS 12.0+, npm 6+
  - Prefer installing it with [nvm for Mac and Linux](https://github.com/creationix/nvm) / [nvm for Windows](https://github.com/coreybutler/nvm-windows)
* Angular 8+
* Ionic 5+

## Installing

```shell
$ npm install
$ npm run serve:dev # other environments are: qa, staging and prod.

$ # Open http://localhost:4200/ in your browser.
```

## Running tests

```shell
$ npm run test
```

## Running lint

```shell
$ npm run lint
```

## Keeping a Changelog

It is very important for a repository to keep a change log of the improvements, fixes and/or deprecations that are made by the team. We recommend to follow [these](https://keepachangelog.com/en/1.0.0/) directives, read them carefully!
