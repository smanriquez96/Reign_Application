import { TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { Util } from './util';


describe('Util', () => {
  let util: Util;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      providers: [
        Util,
      ]
    }).compileComponents();
    util = TestBed.inject(Util);
  });

  it('should create an instance', () => {
    expect(util).toBeTruthy();
  });
});
