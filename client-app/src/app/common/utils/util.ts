import * as datef from 'date-fns-tz';
import { Injectable } from '@angular/core';

const TIMEZONE = 'America/Santiago';

@Injectable()
export class Util {

  constructor() { }

  public formatDate(date: string, pattern = 'dd/MM/yyyy'): string {
    return datef.format(new Date(date), pattern);
  }
}
