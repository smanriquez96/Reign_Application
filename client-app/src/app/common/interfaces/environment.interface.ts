export interface IEnvironment {
    ENV: string;
    USING_MOCKS: boolean;
    BASE_BACKEND: string;
}
