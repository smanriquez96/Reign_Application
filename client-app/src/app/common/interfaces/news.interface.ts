interface HiglightResult {
    value: string;
    mathLevel: string;
    matchedWords: string[];
    fullyHighlighted?: boolean;

}

interface IHiglightResult {
    author: HiglightResult;
    commentText: HiglightResult;
    storyTitle: HiglightResult;
    storyUrl: HiglightResult;
}

export interface INews {
    _id: string;
    points: number | null;
    createdAt: string;
    title: string | null;
    url: string | null;
    author: string | null;
    storyText: string | null;
    commentText: string | null;
    storId: number | null;
    storyTitle: string | null;
    storyUrl: string | null;
    parentId: number | null;
    createdAtI: number;
    tags: string[] | null;
    objectId: string;
    highlightResult: IHiglightResult | null;
}
