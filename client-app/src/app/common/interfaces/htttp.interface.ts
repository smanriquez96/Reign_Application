import { HttpHeaders, HttpParams } from '@angular/common/http';

export interface IHttpParams {
  method: 'delete' | 'get' | 'put' | 'post' | 'patch';
  url: string;
  data?: any;
  customHeaders?: HttpHeaders;
  customParams?: HttpParams;
  timeoutValue?: number;
}
