import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { HOME_URL } from '@constants/url.constants';

const routes: Routes = [
  {
    path: '',
    redirectTo: HOME_URL,
    pathMatch: 'full'
  },
  {
    path: HOME_URL,
    loadChildren: './pages/home/home.module#HomePageModule'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
