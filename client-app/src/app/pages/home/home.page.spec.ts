import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Util } from '@common/utils/util';
import { IonicModule } from '@ionic/angular';
import { HomePage } from './home.page';
import { NEWS_MOCK } from '@common/mocks/news.mock';
import { INews } from '@interfaces/news.interface';

describe('HomePage', () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [ HomePage ],
      imports: [ HttpClientTestingModule ],
      providers: [ Util ]
    }).compileComponents();

    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    let storageSpy: jasmine.Spy;
    let newsSpy: jasmine.Spy;
    const expected = [
      {
        _id: '1234',
        points: 0,
        createdAt: '2021-07-15T10:49:08.000Z',
        title: 'news title',
        url: 'https://example-url.com',
        author: 'News author',
        storyText: null,
        commentText: null,
        storId: 3213213,
        storyTitle: 'node js news',
        storyUrl: 'https://example-url.com',
        parentId: 345678,
        createdAtI: 1626346148,
        tags: [ ' story' ],
        objectId: '32163',
        highlightResult: null,
      },
      {
          _id: '1235',
          points: 0,
          createdAt: '2021-07-15T10:49:08.000Z',
          title: 'news title',
          url: 'https://example-url.com',
          author: 'News author',
          storyText: null,
          commentText: null,
          storId: 3213213,
          storyTitle: 'node js news',
          storyUrl: 'https://example-url.com',
          parentId: 345678,
          createdAtI: 1626346148,
          tags: [ ' story' ],
          objectId: '32163',
          highlightResult: null,
        },
        {
          _id: '1236',
          points: 0,
          createdAt: '2021-07-15T10:49:08.000Z',
          title: 'news title',
          url: 'https://example-url.com',
          author: 'News author',
          storyText: null,
          commentText: null,
          storId: 3213213,
          storyTitle: 'node js news',
          storyUrl: 'https://example-url.com',
          parentId: 345678,
          createdAtI: 1626346148,
          tags: [ ' story' ],
          objectId: '32163',
          highlightResult: null,
        }
    ];

    beforeEach(() => {
      storageSpy = spyOn(component['storageService'], 'get');
      newsSpy = spyOn(component['newsService'], 'getNews');
    });

    it('should set the news property', fakeAsync(() => {
      storageSpy.and.returnValue(undefined);
      newsSpy.and.returnValue(Promise.resolve([...NEWS_MOCK]));
      component.ngOnInit();
      tick();
      expect(component.news).toEqual(expected);
      expect(component.news.length).toEqual(expected.length);
    }));

    it('should set the news property and filter it by the removed ones', fakeAsync(() => {
      storageSpy.and.returnValue(['1234']);
      newsSpy.and.returnValue(Promise.resolve([...NEWS_MOCK]));
      const filteredExpected = expected.slice(1);
      component.ngOnInit();
      tick();
      expect(component.news).toEqual(filteredExpected);
      expect(component.news.length).toEqual(expected.length - 1);
      expect(component.removedNewsIds).toEqual(['1234']);
    }));

    it('should set the news property to an empty array', fakeAsync(() => {
      storageSpy.and.returnValue([]);
      newsSpy.and.returnValue(Promise.reject());
      component.ngOnInit();
      tick();
      expect(component.news.length).toEqual(0);
    }));
  });

  describe('getNewsTitle', () => {
    it('should return the news story title', () => {
      const title = 'node js news';
      const news = { storyTitle: title } as INews;
      expect(component.getNewsTitle(news)).toEqual('node js news');
    });

    it('should return the news title', () => {
      const title = 'node js news';
      const news = { storyTitle: null, title } as INews;
      expect(component.getNewsTitle(news)).toEqual('node js news');
    });
  });

  describe('openStoryUrl', () => {
    it('should open a new window with the story url', () => {
      spyOn(window, 'open').and.callFake(() => window);
      const url = 'https:///www.google.com';
      const news = { storyUrl: url } as INews;
      component.openStoryUrl(news);
      expect(window.open).toHaveBeenCalled();
      expect(window.open).toHaveBeenCalledWith(url);
    });

    it('should open a new window with the url', () => {
      spyOn(window, 'open').and.callFake(() => window);
      const url = 'https:///www.google.com';
      const news = { storyUrl: null, url } as INews;
      component.openStoryUrl(news);
      expect(window.open).toHaveBeenCalled();
      expect(window.open).toHaveBeenCalledWith(url);
    });

    it('should not open a new window', () => {
      spyOn(window, 'open').and.callFake(() => window);
      const url = 'https:///www.google.com';
      const news = { storyUrl: null, url: null } as INews;
      component.openStoryUrl(news);
      expect(window.open).not.toHaveBeenCalled();
    });
  });

  describe('removeNews', () => {
    it('should remove the selected news and save the id on the storage service', () => {
      const storageSpy = spyOn(component['storageService'], 'set');
      component.news = [ ...NEWS_MOCK ];
      component.removedNewsIds = [];
      const expectedLength = component.news.length - 1;
      const toRemove = { _id: '1236' } as INews;
      const filteredExpected = NEWS_MOCK.filter((news) => news._id !== '1236');
      component.removeNews(toRemove);
      expect(storageSpy).toHaveBeenCalledWith('removedNewsIds', ['1236']);
      expect(component.news.length).toEqual(expectedLength);
      expect(component.news).toEqual(filteredExpected);
    });
  });
});
