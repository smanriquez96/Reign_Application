import { Component, OnInit } from '@angular/core';
import { Util } from '@common/utils/util';
import { INews } from '@interfaces/news.interface';
import { NewsService } from '@services/news/news.service';
import { StorageService } from '@services/storage/storage.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  public news: INews[] = [];
  public removedNewsIds: string[] = [];
  public loadingNews = true;

  constructor(
    private readonly newsService: NewsService,
    private readonly util: Util,
    private storageService: StorageService
  ) { }

  public ngOnInit(): void {
    this.loadingNews = true;
    this.removedNewsIds =  this.storageService.get('removedNewsIds') || [];
    this.newsService.getNews()
    .then((response) => {
      this.news = response.filter((news) => !this.removedNewsIds.includes(news._id));
    })
    .catch(() => this.news = [])
    .finally(() => this.loadingNews = false);
  }

  public getNewsTitle(news: INews): string {
    const title = news.storyTitle ? news.storyTitle : news.title ? news.title : '';
    return title;
  }

  public getNewsDate(news: INews): string {
    const today = new Date();
    const newsDate = new Date(news.createdAt);
    const dayDifference = today.getDay() - newsDate.getDay();
    switch (dayDifference) {
      case 0:
        return this.util.formatDate(news.createdAt, 'HH:MM aaaaa\'m\'');
      case 1:
        return 'Yesterday';
      default:
        return this.util.formatDate(news.createdAt, 'MMMM dd');
    }
  }

  public openStoryUrl(news: INews): void {
    const url = news.storyUrl ? news.storyUrl : news.url;
    if (url) { window.open(url); }
  }

  public removeNews(newstoRemove: INews): void {
    this.news = this.news.filter((news) => news._id !== newstoRemove._id);
    this.removedNewsIds.push(newstoRemove._id);
    this.storageService.set('removedNewsIds', this.removedNewsIds);
  }
}
