import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpService } from './http.service';
import { of } from 'rxjs';

describe('HttpService', () => {
  let service: HttpService;
  let httpGetSpy: jasmine.Spy;
  let httpDeleteSpy: jasmine.Spy;
  let httpPostSpy: jasmine.Spy;
  let httpPutSpy: jasmine.Spy;
  let httpPatchSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [
        HttpService
      ],
    });
    service = TestBed.inject(HttpService);
    httpGetSpy = spyOn(service['http'], 'get').and.returnValue(of());
    httpDeleteSpy = spyOn(service['http'], 'delete').and.returnValue(of());
    httpPostSpy = spyOn(service['http'], 'post').and.returnValue(of());
    httpPutSpy = spyOn(service['http'], 'put').and.returnValue(of());
    httpPatchSpy = spyOn(service['http'], 'patch').and.returnValue(of());
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('Public methods get, delete, post, and put', () => {
    it('should return the httpClient call', async () => {
      const responseGet = service.get('url');
      httpGetSpy.and.returnValue(of(true));
      responseGet.subscribe((result) => expect(result).toEqual(true));
      expect(httpGetSpy).toHaveBeenCalled();

      const responseDelete = service.delete('url');
      httpDeleteSpy.and.returnValue(of(true));
      responseDelete.subscribe((result) => expect(result).toEqual(true));
      expect(httpDeleteSpy).toHaveBeenCalled();

      const responsePost = service.post('url', {});
      httpPostSpy.and.returnValue(of(true));
      responsePost.subscribe((result) => expect(result).toEqual(true));
      expect(httpPostSpy).toHaveBeenCalled();

      const responsePatch = service.patch('url', {});
      httpPatchSpy.and.returnValue(of(true));
      responsePatch.subscribe((result) => expect(result).toEqual(true));
      expect(httpPatchSpy).toHaveBeenCalled();

      const responsePut = service.put('url', {});
      httpPutSpy.and.returnValue(of(true));
      responsePut.subscribe((result) => expect(result).toEqual(true));
      expect(httpPutSpy).toHaveBeenCalled();
    });
  });
});
