import { Observable } from 'rxjs';
import { timeout } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpParams, } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IHttpParams } from '@interfaces/htttp.interface';
import { DEFAULT_SERVICE_TIMEOUT } from '@constants/business.constants';

@Injectable({
  providedIn: 'root'
})

export class HttpService {
  private headers = new HttpHeaders();
  private params = new HttpParams();

  constructor(private http: HttpClient) { }


  private httpCall({ method, url, data, customHeaders, timeoutValue, customParams }: IHttpParams): Observable<any> {
    const header = customHeaders ? customHeaders : this.headers;
    const params = customParams ? customParams : this.params;
    timeoutValue = timeoutValue ? timeoutValue : DEFAULT_SERVICE_TIMEOUT;

    if (method === 'delete' || method === 'get') { return this.http[method]<any>(url, { headers: header, params })
      .pipe(timeout(timeoutValue));
    }

    return this.http[method]<any>(url, data, { headers: header }).pipe(timeout(timeoutValue));
  }

  public delete(url: string, customHeaders?: HttpHeaders): Observable<any> {
    return this.httpCall({ method: 'delete', url, data: false, customHeaders });
  }

  public get(url: string, customParams?: HttpParams, customHeaders?: HttpHeaders): Observable<any> {
    return this.httpCall({ method: 'get', url, customParams, data: false, customHeaders });
  }

  public post(url: string, data: any, customHeaders?: HttpHeaders): Observable<any> {
    return this.httpCall({ method: 'post', url, data, customHeaders });
  }

  public put(url: string, data: any, customHeaders?: HttpHeaders): Observable<any> {
    return this.httpCall({ method: 'put', url, data, customHeaders });
  }

  public patch(url: string, data: any, timeoutSetting?: number, customHeaders?: HttpHeaders): Observable<any> {
    return this.httpCall({ method: 'patch', url, data, timeoutValue: timeoutSetting, customHeaders });
  }
}
