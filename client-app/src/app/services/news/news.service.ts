import { Injectable } from '@angular/core';
import { environment } from '@env';
import { HttpService } from '@services/http/http.service';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(
    private http: HttpService,
  ) { }

  public getNews(): Promise<any[]> {
    const url = `${environment.BASE_BACKEND}news`;
    return this.http.get(url).toPromise() as Promise<any[]>;
  }
}
