import { TestBed } from '@angular/core/testing';
import { NewsService } from './news.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { INews } from '@common/interfaces/news.interface';
import { of } from 'rxjs';
import { NEWS_MOCK_ONE } from '@common/mocks/news.mock';

describe('NewsService', () => {
  let service: NewsService;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ NewsService ]
    });
    service = TestBed.inject(NewsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('get', () => {
    const expected = [
      {
        _id: '372187362918312',
        points: 0,
        createdAt: '2021-07-15T10:49:08.000Z',
        title: 'news title',
        url: 'https://example-url.com',
        author: 'News author',
        storyText: null,
        commentText: null,
        storId: 3213213,
        storyTitle: 'node js news',
        storyUrl: 'https://example-url.com',
        parentId: 345678,
        createdAtI: 1626346148,
        tags: [ ' story' ],
        objectId: '32163',
        highlightResult: null,
      }
    ] as INews[];

    it('should call get method to get news', async () => {
      const getSpy = spyOn(service['http'], 'get').and.returnValue(of(NEWS_MOCK_ONE));
      const response = await service.getNews();
      expect(getSpy).toHaveBeenCalled();
      expect(response).toEqual(expected);
    });
  });
});
