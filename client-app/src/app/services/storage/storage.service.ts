import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  // tslint:disable-next-line: variable-name
  private _data = sessionStorage;

  constructor() { }

  public clear(): void {
    this._data.clear();
  }

  public get(id: string): any {
    const element = this._data.getItem(id);
    if (element !== null) { return JSON.parse(element); }
  }

  public set(id: string, data: any): void {
    this._data.setItem(id, JSON.stringify(data));
  }

  public remove(id: string): void {
    this._data.removeItem(id);
  }
}
