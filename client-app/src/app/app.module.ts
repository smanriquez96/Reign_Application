import { IonicModule } from '@ionic/angular';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { ComponentsModule } from '@components/components.module';
import { AppComponent } from './app.component';
import { NewsService } from '@services/news/news.service';
import { HttpService } from '@services/http/http.service';
import { HttpClientModule } from '@angular/common/http';
import { Util } from '@common/utils/util';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    ComponentsModule,
    HttpClientModule,
    IonicModule.forRoot()
  ],
  providers: [
    HttpService,
    NewsService,
    Util
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }
