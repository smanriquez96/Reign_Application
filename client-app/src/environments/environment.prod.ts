import { IEnvironment } from '@interfaces/environment.interface';

export const environment: IEnvironment = {
  ENV: 'production',
  USING_MOCKS: false,
  BASE_BACKEND: ''
};
