import { IEnvironment } from '@interfaces/environment.interface';

export const environment: IEnvironment = {
  ENV: 'development',
  USING_MOCKS: false,
  BASE_BACKEND: 'http://localhost:8080/'
};
