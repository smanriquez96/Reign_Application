import { IEnvironment } from '@interfaces/environment.interface';

export const environment: IEnvironment = {
  ENV: 'qa',
  USING_MOCKS: false,
  BASE_BACKEND: ''
};
