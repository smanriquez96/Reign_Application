import { IEnvironment } from '@interfaces/environment.interface';

export const environment: IEnvironment = {
  ENV: '',
  USING_MOCKS: false,
  BASE_BACKEND: ''
};
